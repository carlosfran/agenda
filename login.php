<?php
	require 'controlador.php';
	$control = new Controlador();
	$control->verificarLogin();
	
	require_once 'partes/cabecalho.php';
	require_once 'partes/menu.php';
?>
<div class="row">
	<div>
		<h3>Entrar</h3>
		<?php
			if(isset($_POST['msg'])){
				print $_POST['msg'];
			}
		?>
		<form action="" method="POST">
			<label for="email">E-mail*</label>
			<input type="email" name="email"><br/>
			
			<label for="senha">Senha*</label>
			<input type="password" name="senha"><br/>
			
			<input type="submit" value="Entrar">
		</form>
	<div>
<div>
<?php
	include 'partes/rodape.php';
?>