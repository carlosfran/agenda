<?php
	require 'controlador.php';
	$control = new Controlador();
	$control->cadastrarUsuario();
	
	require_once 'partes/cabecalho.php';
	require_once 'partes/menu.php';
?>
<div class="row">
	<div>
		<h3>Cadastro</h3>
		<?php
			if(isset($_POST['msg'])){
				print $_POST['msg'];
			}
		?>
		<form action="" method="POST">
			<label for="nome">Nome*</label>
			<input value="" type="text" name="nome"><br/>
			
			<label for="email">E-mail*</label>
			<input type="text" name="email"><br/>
			
			<label for="senha">Senha*</label>
			<input type="password" name="senha"><br/>
			
			<label for="bio">Bio</label><br/>
			<textarea name="bio" cols="" rows=""></textarea><br/>
			
			<input type="submit" value="Cadastrar">
		</form>
	<div>
<div>
<?php
	include 'partes/rodape.php';
?>