<?php
require 'db/config.php';

class Modelo{
	private $server;
	private $user;
	private $passwd;
	private $porta;
	private $dbname;
	private $con;
	
	public function __construct()
	{
		global $server;
		global $user;
		global $passwd;
		global $porta;
		global $dbname;
		$this->server = $server;
		$this->user = $user;
		$this->passwd = $passwd;
		$this->porta = $porta;
		$this->dbname = $dbname;
	}
	
	public function conectar()
	{
		$url = "mysql:host=$this->server;
		dbname=$this->dbname";
		try{
			$this->con = new PDO(
				$url, $this->user, $this->passwd);
			$this->con->setAttribute(
				PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION
			);
			return true;
		}catch(PDOException $e){
			// mostra mensagem
			print_r($e->getMessage());
			return false;
		}
	}
	
	public function cadastrarUsuario($nome, $email, $senha, $bio)
	{
		$ret = 0;
		$tipo=0;
		$sql = "INSERT INTO Usuario
			(nome, email, senha, bio, tipo) VALUES(?,?,sha2(?, 512),?,?)";
		if(  $this->conectar() ){
			// executar comandos sql
			$stmt = $this->con->prepare($sql);
			$stmt->bindParam(1, $nome, PDO::PARAM_STR);
			$stmt->bindParam(2, $email, PDO::PARAM_STR);
			$stmt->bindParam(3, $senha, PDO::PARAM_STR);
			$stmt->bindParam(4, $bio, PDO::PARAM_STR);
			$stmt->bindParam(5, $tipo, PDO::PARAM_INT);
			if( $stmt->execute() )
				$ret = $this->con->lastInsertId();
		}
		return $ret;
	}
	
	public function cadastrarContato(
		$nome, $email, $telefone, $usuario_id)
	{
		$ret = 0;
		$sql = "INSERT INTO Contato
			(nome, email, telefone, Usuario_idUsuario)
			VALUES(?,?,?,?)";
		if(  $this->conectar() ){
			// executar comandos sql
			$stmt = $this->con->prepare($sql);
			$stmt->bindParam(1, $nome, PDO::PARAM_STR);
			$stmt->bindParam(2, $email, PDO::PARAM_STR);
			$stmt->bindParam(3, $telefone, PDO::PARAM_STR);
			$stmt->bindParam(4, $usuario_id, PDO::PARAM_INT);
			if( $stmt->execute() )
				$ret = $this->con->lastInsertId();
		}
		return $ret;
	}
	
	public function verificarLogin($email, $senha)
	{
		$user = null;
		$sql = "SELECT * FROM Usuario WHERE email=? AND senha=sha2(?, 512)";
		if(  $this->conectar() ){
			// executar comandos sql
			$stmt = $this->con->prepare($sql);
			$stmt->bindParam(1, $email, PDO::PARAM_STR);
			$stmt->bindParam(2, $senha, PDO::PARAM_STR);
			if( $stmt->execute() ){
				while($row = $stmt->fetch(PDO::FETCH_ASSOC)){
					$user = array();
					$user['idUsuario'] = $row['idUsuario'];
					$user['nome'] = $row['nome'];
					$user['email'] = $row['email'];
					$user['bio'] = $row['bio'];
					$user['tipo'] = $row['tipo'];
				}
			}
		}
		return $user;
	}
	
	public function getUsuario($email)
	{
		$user = null;
		$sql = "SELECT * FROM Usuario WHERE email=?";
		if(  $this->conectar() ){
			// executar comandos sql
			$stmt = $this->con->prepare($sql);
			$stmt->bindParam(1, $email, PDO::PARAM_STR);
			if( $stmt->execute() ){
				while($row = $stmt->fetch(PDO::FETCH_ASSOC)){
					$user = array();
					$user['idUsuario'] = $row['idUsuario'];
					$user['nome'] = $row['nome'];
					$user['email'] = $row['email'];
					$user['bio'] = $row['bio'];
					$user['tipo'] = $row['tipo'];
				}
			}
		}
		return $user;
	}
	
	public function listarContatos($usuario_id)
	{
		$contatos = null;
		$sql = "SELECT * FROM Contato
			WHERE Usuario_idUsuario=?";
		if(  $this->conectar() ){
			// executar comandos sql
			$stmt = $this->con->prepare($sql);
			$stmt->bindParam(1, $usuario_id, PDO::PARAM_INT);
			if( $stmt->execute() ){
				$contatos = $stmt->fetchAll();
			}
		}
		return $contatos;
	}
	
	public function listarUsuarios()
	{
		$usuarios = null;
		$sql = "SELECT idUsuario, nome, email, bio, tipo
			FROM Usuario";
		if(  $this->conectar() ){
			// executar comandos sql
			$stmt = $this->con->prepare($sql);
			if( $stmt->execute() ){
				$usuarios = $stmt->fetchAll();
			}
		}
		return $usuarios;
	}
}
?>






