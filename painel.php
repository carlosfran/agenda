<?php
	require 'controlador.php';
	$control = new Controlador();
	$control->checkLogin();
	
	$acao = null;
	if( isset($_GET['acao']) )
		$acao = $_GET['acao'];
	
	$conteudo = 'partes/painel-index.php';
	switch($acao){
		case 'cadastrar':
			if($_SERVER['REQUEST_METHOD'] == 'POST'){
				$control->cadastrarContato();
			}
			$conteudo = 'partes/form-contato.php';
			break;
		case 'listar':
			$control->listarContatos();
			$conteudo = 'partes/table-contato.php';
			break;
		case 'usuarios':
			$control->listarUsuarios();
			$conteudo = 'partes/table-usuarios.php';
			break;
		case 'contatos':
			$control->listarContatosUsuario();
			$conteudo = 'partes/table-contato.php';
			break;
	}
	require_once 'partes/cabecalho.php';
	require_once 'partes/menu.php';
?>
<div class="row">
	<h3>Painel</h3>
	<p>Olá, <?= $_SESSION['usuario']['nome']; ?>!</p>
	<?= $_SESSION['usuario']['tipo']; ?>
</div>
<div class="row">
	<!-- inicio:conteudo -->
	<?php include $conteudo; ?>
	<!-- fim:conteudo -->
</div>
<?php include 'partes/rodape.php'; ?>
