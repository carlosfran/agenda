<?php
session_start();
require 'modelo.php';
require 'suapapi.php';

class Controlador{
	private $modelo;
	
	public function __construct()
	{
		// criar o modelo
		$this->modelo = new Modelo();
	}
	
	public function cadastrarUsuario()
	{
		if($_SERVER['REQUEST_METHOD'] == 'POST'){
			if( !empty($_POST['nome']) &&
				!empty($_POST['email']) &&
				!empty($_POST['senha'])){
					
				$id = $this->modelo
					->cadastrarUsuario($_POST['nome'],
					$_POST['email'], $_POST['senha'], $_POST['bio']);
				if($id > 0)
					$_POST['msg'] = '<p><strong>Cadastrado com sucesso! ID = '.$id.'</strong></p>';
				else
					$_POST['msg'] = '<p><strong>Erro ao cadastrar!</strong></p>';
			}else{
				$_POST['msg'] = '<p><strong>Preencher campos obrigatórios!</strong></p>';
			}
		}
	}
	
	public function cadastrarContato()
	{
		if($_SERVER['REQUEST_METHOD'] == 'POST'){
			if( !empty($_POST['nome']) &&
				!empty($_POST['telefone'])){
					
				$id = $this->modelo
					->cadastrarContato($_POST['nome'],
					$_POST['email'], $_POST['telefone'],
					$_SESSION['usuario']['idUsuario']);
				if($id > 0)
					$_POST['msg'] = '<p><strong>Cadastrado com sucesso! ID = '.$id.'</strong></p>';
				else
					$_POST['msg'] = '<p><strong>Erro ao cadastrar!</strong></p>';
			}else{
				$_POST['msg'] = '<p><strong>Preencher campos obrigatórios!</strong></p>';
			}
		}
	}
	
	public function verificarLogin()
	{
		if($_SERVER['REQUEST_METHOD'] == 'POST'){
			if(	!empty($_POST['email']) &&
				!empty($_POST['senha'])){
					
				$ret = $this->modelo
					->verificarLogin($_POST['email'],$_POST['senha']);
				if(isset($ret))
				{
					// registrar na sessao
					$_SESSION['usuario'] = $ret;
					// redirecionar para o painel
					header('Location: painel.php');
				}else
					$_POST['msg'] = '<p><strong>E-mail e/ou senha incorreto!</strong></p>';
			}else{
				$_POST['msg'] = '<p><strong>Preencher campos obrigatórios!</strong></p>';
			}
		}
	}
	
	public function loginSUAP()
	{
		if($_SERVER['REQUEST_METHOD'] == 'POST'){
			if(	!empty($_POST['matricula']) &&
				!empty($_POST['senha'])){
				
				// via SUAP
				$suap = new Suapapi();
				$token = $suap->autentica(
					$_POST['matricula'], $_POST['senha']);
				$user_suap = null;				
				if(isset($token)){
					$user_suap = $suap->getMeusDados($token);
					$user_db = $this->modelo->
						getUsuario($user_suap->email);
					$cad = null;
					if(!isset($user_db)){
						$cad = $this->modelo->cadastrarUsuario(
							$user_suap->nome_usual,
							$user_suap->email,
							$token, "");
					}
					if(isset($cad) || isset($user_db)){
						$_SESSION['usuario'] = $this->
							modelo->getUsuario($user_suap->email);
						header('Location: painel.php');
					}else{
						$_POST['msg'] = '<p><strong>Impossível efetuar login!</strong></p>';
					}
				}else{
					$_POST['msg'] = '<p><strong>Matrícula/senha incorreto(s)!</strong></p>';
				}
			}
		}
	}
	
	public function listarContatos(){
		$_POST['contatos'] = $this->modelo->listarContatos(
			$_SESSION['usuario']['idUsuario']);
	}
	
	public function listarContatosUsuario(){
		$this->checkPermissao('contatos');
		if( isset($_GET['id'] )){
			$_POST['contatos'] = $this->modelo->listarContatos($_GET['id']);
		}
	}
	
	public function listarUsuarios(){
		$this->checkPermissao('usuarios');
		$_POST['usuarios'] = $this->modelo->listarUsuarios();
	}
	
	public function checkLogin()
	{
		if(!isset($_SESSION['usuario']) )
		{
			header('Location: login.php');
		}
	}
	
	private function checkPermissao($acao){
		$perm = array(
			'usuarios' => 1,
			'listar' => 0,
			'cadastrar' => 0,
			'contatos' => 1
		);
		
		if(isset($_SESSION['usuario']) &&
			$_SESSION['usuario']['tipo'] >= $perm[$acao] )
			return true;
		else
			header('Location: painel.php');
	}
}
?>




