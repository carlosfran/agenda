-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

-- -----------------------------------------------------
-- Schema agendadb
-- -----------------------------------------------------

-- -----------------------------------------------------
-- Schema agendadb
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `agendadb` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci ;
USE `agendadb` ;

-- -----------------------------------------------------
-- Table `agendadb`.`Usuario`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `agendadb`.`Usuario` (
  `idUsuario` BIGINT NOT NULL AUTO_INCREMENT,
  `nome` VARCHAR(100) NOT NULL,
  `email` VARCHAR(250) NOT NULL,
  `senha` VARCHAR(250) NOT NULL,
  `bio` TEXT NULL,
  PRIMARY KEY (`idUsuario`),
  UNIQUE INDEX `email_UNIQUE` (`email` ASC))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `agendadb`.`Contato`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `agendadb`.`Contato` (
  `idContato` BIGINT NOT NULL AUTO_INCREMENT,
  `nome` VARCHAR(100) NOT NULL,
  `telefone` VARCHAR(20) NOT NULL,
  `email` VARCHAR(250) NULL,
  `Usuario_idUsuario` BIGINT NOT NULL,
  PRIMARY KEY (`idContato`),
  INDEX `fk_Contato_Usuario_idx` (`Usuario_idUsuario` ASC),
  CONSTRAINT `fk_Contato_Usuario`
    FOREIGN KEY (`Usuario_idUsuario`)
    REFERENCES `agendadb`.`Usuario` (`idUsuario`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
