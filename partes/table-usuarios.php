<div>
	<h3>Usuários cadastrados</h3>
	<table border="2">
		<thead>
			<tr>
				<th>ID</th>
				<th>Nome</th>
				<th>E-mail</th>
				<th>Bio</th>
				<th>Tipo</th>
				<th>Opções</th>				
			</tr>
		</thead>
		<tbody>
			<?php
				if(isset($_POST['usuarios']))
				foreach($_POST['usuarios'] as $c){
			?>
			<tr>
				<td><?= $c['idUsuario']; ?></td>
				<td><?= $c['nome']; ?></td>
				<td><?= $c['email']; ?></td>
				<td><?= $c['bio']; ?></td>
				<td><?= $c['tipo']; ?></td>
				<td>Editar | Excluir | 
				<a href="painel.php?acao=contatos&id=<?= $c['idUsuario']; ?>">Listar contatos</a></td>
			</tr>
				<?php } ?>
		</tbody>
	</table>
</div>