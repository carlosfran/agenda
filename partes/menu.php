<div class="row">
	<!-- menu -->
	<div>
		<ul>
			<li><a href="index.php" title="Página Inicial">Página Inicial</a></li>

			<?php if( isset($_SESSION['usuario']) ){ ?>
			<li><a href="painel.php?acao=cadastrar" title="Cadastrar contato">Cadastrar contato</a></li>
			<li><a href="painel.php?acao=listar" title="Listar contato">Listar contato</a></li>
				<?php if( $_SESSION['usuario']['tipo'] == 1){ ?>
					<li><a href="painel.php?acao=usuarios" title="Listar usuários">Listar usuários</a></li>
				<?php } ?>
			<li><a href="logout.php" title="Sair">Sair</a></li>
			<?php } else { ?>
			<li><a href="cadastro.php" title="Cadastre-se">Cadastre-se</a></li>
			<li><a href="login.php" title="Entrar">Entrar</a></li>
			<li><a href="login-suap.php" title="Login via SUAP">
			Login via SUAP</a></li>
			<?php } ?>
		</ul>
	</div>
</div>